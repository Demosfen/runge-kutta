# Runge Kutta

This is a routine which allows users trace magnetic field lines from magnetic equatorial plane location to ionosphere / Earth&#39;s surface. Tracing method is based on Runge Kutta algorithm, which accelerates calculation time.