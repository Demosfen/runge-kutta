PROGRAM RK4
       
C      --------- Dipole components in (XYZ) sc ---------
C      BX = -Be(Re^3)/(r^5) * (3xz)
C      BY = -Be(Re^3)/(r^5) * (3yz)
C      BZ = -Be(Re^3)/(r^5) * (3(z^2)-(r^2))

       IMPLICIT REAL*8 (A-H,O-Z)
       
       PARAMETER (n=10000)
       REAL, DIMENSION(n+1) :: R
       REAL, DIMENSION(n+1) :: BX , BY , BZ
       REAL, DIMENSION(n+1) :: XX , YY , ZZ
       REAL X,Y,Z,R0,Rmax,d,dir
       
       open(unit=9,file='XYZ_RK4.dat')
       d = 0.05D0                         ! Coordinates step
       DIR=1.                               ! Trace direction
       R0 = 1.D0
       Rmax = 40.D0
       X=7. 
       Y=0.
       Z=0.                  ! Initial point
       
        DO WHILE (X.ge.-7)
       
       call trace_rk4(i,X,Y,Z,XX,YY,ZZ,n,d,R0,Rmax,DIR)
       write(9,'(3F12.5)') 
     *(XX(j),YY(j),ZZ(j),j=1.,i)
       DIR=DIR*(-1)
       call trace_rk4(i,X,Y,Z,XX,YY,ZZ,n,d,R0,Rmax,DIR)
       write(9,'(3F12.5)') 
     *(XX(j),YY(j),ZZ(j),j=1.,i)

        X=X-1.
        END DO
       CLOSE(UNIT=9)
       
       END
       
C      ************************************************************************
C      ************************************************************************
C      ************************************************************************
C      ************************************************************************
C      ************************************************************************

       SUBROUTINE TRACE_rk4(i,X,Y,Z,XX,YY,ZZ,n,d,R0,Rmax,DIR)
       
C      *********************************************************************
C      Runge-Kutta method (4th order)
C      Yn+1 = Yn + f(Xn + 0.5*dx, Yn + 0.5*fn*dx)
C      KSI = ??????
C      *********************************************************************

       use ieee_arithmetic
       REAL, DIMENSION(n+1) :: R
       REAL, DIMENSION(n+1) :: BX , BY , BZ
       REAL, DIMENSION(n+1) :: XX , YY , ZZ
       INTEGER xtoz
       REAL BX1, BY1, BZ1, R1
       REAL X,Y,Z,R0,Rmax,d,dir,k1,k2,k3,k4
       REAL X12,Y12,Z12
       DATA Be /31000.D0/                  ! Equatorial magnetic field
       DATA Re /1.D0/                 ! Earth radii in km [Re]
       
       k1=0.; k2=0.; k3=0.; k4=0.
       X12=0.; Y12=0.; Z12=0.; 
       xtoz=0
       BX=0; BY=0; BZ=0; R=0
       i=1
       XX(i)=X
       YY(i)=Y
       ZZ(i)=Z
       R(i)= R1(X,Y,Z)
       BX(i)=BX1(X,Y,Z)
       BY(i)=BY1(X,Y,Z)
       BZ(i)=BZ1(X,Y,Z)

              DO WHILE (i .le. n)

C      Calculation of Y-coordinate
       k1=BY(i)/BX(i)                                    ! k1 = f(Xn,Yn)
       X12=XX(i)+0.5D0*d                                 ! X12 = Xn + 0.5dX
       Y12=YY(i)+0.5D0*d*k1                              ! Y12 = Yn + 0.5dX*k1
       k2=BY1(X12,Y12,ZZ(i)) / BX1(X12,Y12,ZZ(i))       ! k2 = f(Xn + 0.5dX ; Yn + 0.5dX*k1)
       YY(i+1)=YY(i)+k2*d                                ! Yn+1 = Yn + k2*dX ; Xn+1 = Xn + dX

C       Calculation of Z-coordinate
       k1=BZ(i)/BX(i)                                    ! k1 = f(Zn,Yn)
       Z12=ZZ(i)+0.5D0*d*k1                          ! Z12 = Zn + 0.5dX*k1
       k2=BZ1(X12,YY(i),Z12) / BX1(X12,YY(i),Z12)     ! k2 = f(Xn + 0.5dX ; Zn + 0.5dX*k1)
       ZZ(i+1)=ZZ(i)+k2*d                               ! Zn+1 = Zn + k2*dX ; Xn+1 = Xn + dX
       XX(i+1)=XX(i)+d

       if (ieee_is_nan(YY(i+1)).or.ieee_is_nan(ZZ(i+1))
     *.or.ieee_is_nan(XX(i+1))) then
       xtoz=1.D0
       d=sign(d,BZ(i))*dir
       end if

       if (abs(BZ(i)).gt.abs(BX(i))) then
       xtoz=1.D0
       d=sign(d,BZ(i))*dir
       end if

       if (abs(BZ(i)).le.abs(BX(i))) then
       xtoz=0.D0
       d=sign(d,BX(i))*dir
       end if

       if (xtoz.eq.1) then

C      Calculation of Y-coordinate
       k1=BX(i)/BZ(i)                                    ! k1 = f(Xn,Yn)
       Z12=ZZ(i)+0.5D0*d                                 ! X12 = Xn + 0.5dX
       X12=XX(i)+0.5D0*d*k1                              ! Y12 = Yn + 0.5dX*k1
       k2=BX1(X12,YY(i),Z12) / BZ1(X12,YY(i),Z12)       ! k2 = f(Xn + 0.5dX ; Yn + 0.5dX*k1)
       XX(i+1)=XX(i)+k2*d                                ! Yn+1 = Yn + k2*dX ; Xn+1 = Xn + dX

C       Calculation of Z-coordinate
       k1=BY(i)/BZ(i)                                    ! k1 = f(Zn,Yn)
       Y12=YY(i)+0.5D0*d*k1                          ! Z12 = Zn + 0.5dX*k1
       k2=BY1(XX(i),Y12,Z12) / BZ1(XX(i),Y12,Z12)     ! k2 = f(Xn + 0.5dX ; Zn + 0.5dX*k1)
       YY(i+1)=YY(i)+k2*d                               ! Zn+1 = Zn + k2*dX ; Xn+1 = Xn + dX
       ZZ(i+1)=ZZ(i)+d

              goto 1

       end if

1      i=i+1

       R(i)= R1(XX(i),YY(i),ZZ(i))
       BX(i)=BX1(XX(i),YY(i),ZZ(i))
       BY(i)=BY1(XX(i),YY(i),ZZ(i))
       BZ(i)=BZ1(XX(i),YY(i),ZZ(i))

       if ((R(i).le.R0).or.(R(i).ge.Rmax)) exit

       END DO

       RETURN
       END

C      ************************************************************************
       FUNCTION R1(X,Y,Z)
!        REAL X,Y,Z
       R1=sqrt(X**2+Y**2+Z**2)
       END
C      ************************************************************************
       FUNCTION BX1(X,Y,Z)
!        REAL X,Y,Z,Be,Re
!        REAL R1
       DATA Be /31000.D0/                  ! Equatorial magnetic field
       DATA Re /1.D0/                 ! Earth radii in km [Re]
       BX1=-1*Be*Re**3 / R1(X,Y,Z)**5 * 3*X*Z
       END
C      ************************************************************************
       FUNCTION BY1(X,Y,Z)
!        REAL X,Y,Z,Be,Re
!        REAL R1
       DATA Be /31000.D0/                  ! Equatorial magnetic field
       DATA Re /1.D0/                 ! Earth radii in km [Re]
       BY1=-1*Be*Re**3 / R1(X,Y,Z)**5 * 3*Y*Z
       END
C      ************************************************************************
       FUNCTION BZ1(X,Y,Z)
       DATA Be /31000.D0/                  ! Equatorial magnetic field
       DATA Re /1.D0/                 ! Earth radii in km [Re]
       BZ1=-1*Be*Re**3 / R1(X,Y,Z)**5 * (3*Z**2 - R1(X,Y,Z)**2)
       END
C      ************************************************************************